# Lavanderia

Enunciado:

La lavandería “Los trajes” desea tener el manejo y el control de las prendas y objetos que ingresan a la lavandería por servicio a realizar. 

El cliente trae sus prendas u objetos para realizar cualquiera de los diferentes servicios que ofrece el cual cada uno tiene un valor en los diferentes tipos de prendas y objetos que es capaz de manipular. 

El sistema maneja los estados de la prenda o el objeto al momento de ser recepcionado, los descuentos que ofrece por cantidad de prendas (en RN). 

Debe de existir la forma de registrar a los usuarios del sistema (con clave y contraseña) y la captura del empleado que realiza cada proceso. 

Un cliente puede tener uno o varios teléfonos y una o varias direcciones que pueden ser en diferentes ciudades del país. 

La lavandería tiene el servicio de entrega a domicilio el cual el cliente determina utilizar o no al momento de dejar sus prendas u objetos y si lo opta, determina a cuál de sus direcciones lo deben de entregar. Los datos en este enunciado son básicos, los deben de complementar acorde a las consultas realizadas acorde problema propuesto.